/* guile-cairo
 * Copyright (C) 2007, 2011, 2012, 2020 Andy Wingo <wingo at pobox dot com>
 *
 * guile-cairo-fo-types.c: Cairo for Guile
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <libguile.h>

#include <cairo.h>

#include "guile-cairo-compat.h"
#include "guile-cairo-fo-types.h"

SCM scm_fo_cairo_t;
SCM scm_fo_cairo_surface_t;
SCM scm_fo_cairo_pattern_t;
SCM scm_fo_cairo_scaled_font_t;
SCM scm_fo_cairo_font_face_t;
SCM scm_fo_cairo_font_options_t;
SCM scm_fo_cairo_path_t;
#if CAIRO_VERSION >= CAIRO_VERSION_ENCODE(1,10,0)
SCM scm_fo_cairo_device_t;
SCM scm_fo_cairo_region_t;
#endif

SCM_GLOBAL_VARIABLE(scm_fo_cairo_var, "<cairo-context>");
SCM_GLOBAL_VARIABLE(scm_fo_cairo_surface_var, "<cairo-surface>");
SCM_GLOBAL_VARIABLE(scm_fo_cairo_pattern_var, "<cairo-pattern>");
SCM_GLOBAL_VARIABLE(scm_fo_cairo_scaled_font_var, "<cairo-scaled-font>");
SCM_GLOBAL_VARIABLE(scm_fo_cairo_font_face_var, "<cairo-font-face>");
SCM_GLOBAL_VARIABLE(scm_fo_cairo_font_options_var, "<cairo-font-options>");
SCM_GLOBAL_VARIABLE(scm_fo_cairo_path_var, "<cairo-path>");
#if CAIRO_VERSION >= CAIRO_VERSION_ENCODE(1,10,0)
SCM_GLOBAL_VARIABLE(scm_fo_cairo_device_var, "<cairo-device>");
SCM_GLOBAL_VARIABLE(scm_fo_cairo_region_var, "<cairo-region>");
#endif

void
already_destroyed (SCM x)
{
  scm_error (scm_from_utf8_symbol ("cairo-error"),
             NULL,
             "Object has been destroyed already: ~S",
             scm_list_1 (x),
             SCM_EOL);
}

/**********************************************************************
 * cairo_t
 **********************************************************************/

SCM
scm_take_cairo (cairo_t *ctx)
{
  SCM sctx;

  sctx = scm_make_foreign_object_1 (scm_fo_cairo_t, ctx);

  return sctx;
}

SCM
scm_from_cairo (cairo_t *ctx)
{
  return scm_take_cairo (cairo_reference (ctx));
}

cairo_t*
scm_to_cairo (SCM scm)
{
  cairo_t *ret;
  scm_assert_foreign_object_type (scm_fo_cairo_t, scm);
  ret = (cairo_t*)scm_foreign_object_ref (scm, 0);
  if (!ret)
    already_destroyed (scm);
  return ret;
}

static void
scm_cairo_free (SCM fo)
{
  cairo_t *ctx = (cairo_t*)scm_foreign_object_ref (fo, 0);

  scm_foreign_object_set_x (fo, 0, NULL);
  if (ctx)
    cairo_destroy (ctx);
}

cairo_t*
scm_release_cairo (SCM scm)
{
  cairo_t *cr = scm_to_cairo (scm);
  scm_cairo_free (scm);
  return cr;
}

/**********************************************************************
 * cairo_surface_t
 **********************************************************************/

SCM
scm_take_cairo_surface (cairo_surface_t *surf)
{
  SCM ssurf;

  ssurf = scm_make_foreign_object_1 (scm_fo_cairo_surface_t, surf);

  return ssurf;
}

SCM
scm_from_cairo_surface (cairo_surface_t *surf)
{
  return scm_take_cairo_surface (cairo_surface_reference (surf));
}

cairo_surface_t*
scm_to_cairo_surface (SCM scm)
{
  cairo_surface_t *ret;
  scm_assert_foreign_object_type (scm_fo_cairo_surface_t, scm);
  ret = (cairo_surface_t*)scm_foreign_object_ref (scm, 0);
  if (!ret)
    already_destroyed (scm);
  return ret;
}

static void
scm_cairo_surface_free (SCM fo)
{
  cairo_surface_t *surf = (cairo_surface_t*)scm_foreign_object_ref (fo, 0);

  scm_foreign_object_set_x (fo, 0, NULL);
  if (surf)
    cairo_surface_destroy (surf);
}

cairo_surface_t*
scm_release_cairo_surface (SCM scm)
{
  cairo_surface_t *cr = scm_to_cairo_surface (scm);
  scm_cairo_surface_free (scm);
  return cr;
}

/**********************************************************************
 * cairo_pattern_t
 **********************************************************************/

SCM
scm_take_cairo_pattern (cairo_pattern_t *pat)
{
  SCM spat;

  spat = scm_make_foreign_object_1 (scm_fo_cairo_pattern_t, pat);

  return spat;
}

SCM
scm_from_cairo_pattern (cairo_pattern_t *pat)
{
  return scm_take_cairo_pattern (cairo_pattern_reference (pat));
}

cairo_pattern_t*
scm_to_cairo_pattern (SCM scm)
{
  scm_assert_foreign_object_type (scm_fo_cairo_pattern_t, scm);
  return (cairo_pattern_t*)scm_foreign_object_ref (scm, 0);
}

static void
scm_cairo_pattern_free (SCM fo)
{
  cairo_pattern_t *pat = (cairo_pattern_t*)scm_foreign_object_ref (fo, 0);

  scm_foreign_object_set_x (fo, 0, NULL);
  cairo_pattern_destroy (pat);
}

/**********************************************************************
 * cairo_font_face_t
 **********************************************************************/

SCM
scm_take_cairo_font_face (cairo_font_face_t *pat)
{
  SCM spat;

  spat = scm_make_foreign_object_1 (scm_fo_cairo_font_face_t, pat);

  return spat;
}

SCM
scm_from_cairo_font_face (cairo_font_face_t *pat)
{
  return scm_take_cairo_font_face (cairo_font_face_reference (pat));
}

cairo_font_face_t*
scm_to_cairo_font_face (SCM scm)
{
  scm_assert_foreign_object_type (scm_fo_cairo_font_face_t, scm);
  return (cairo_font_face_t*)scm_foreign_object_ref (scm, 0);
}

static void
scm_cairo_font_face_free (SCM fo)
{
  cairo_font_face_t *pat = (cairo_font_face_t*)scm_foreign_object_ref (fo, 0);

  scm_foreign_object_set_x (fo, 0, NULL);
  cairo_font_face_destroy (pat);
}

/**********************************************************************
 * cairo_scaled_font_t
 **********************************************************************/

SCM
scm_take_cairo_scaled_font (cairo_scaled_font_t *pat)
{
  SCM spat;

  cairo_scaled_font_reference (pat);
  spat = scm_make_foreign_object_1 (scm_fo_cairo_scaled_font_t, pat);

  return spat;
}

SCM
scm_from_cairo_scaled_font (cairo_scaled_font_t *pat)
{
  return scm_take_cairo_scaled_font (cairo_scaled_font_reference (pat));
}

cairo_scaled_font_t*
scm_to_cairo_scaled_font (SCM scm)
{
  scm_assert_foreign_object_type (scm_fo_cairo_scaled_font_t, scm);
  return (cairo_scaled_font_t*)scm_foreign_object_ref (scm, 0);
}

static void
scm_cairo_scaled_font_free (SCM fo)
{
  cairo_scaled_font_t *pat = (cairo_scaled_font_t*)scm_foreign_object_ref (fo, 0);

  scm_foreign_object_set_x (fo, 0, NULL);
  cairo_scaled_font_destroy (pat);
}

/**********************************************************************
 * cairo_font_options_t
 **********************************************************************/

SCM
scm_take_cairo_font_options (cairo_font_options_t *fopt)
{
  SCM sfopt;

  sfopt = scm_make_foreign_object_1 (scm_fo_cairo_font_options_t, fopt);

  return sfopt;
}

cairo_font_options_t*
scm_to_cairo_font_options (SCM scm)
{
  scm_assert_foreign_object_type (scm_fo_cairo_font_options_t, scm);
  return (cairo_font_options_t*)scm_foreign_object_ref (scm, 0);
}

static void
scm_cairo_font_options_free (SCM fo)
{
  cairo_font_options_t *fopt = (cairo_font_options_t*)scm_foreign_object_ref (fo, 0);

  scm_foreign_object_set_x (fo, 0, NULL);
  cairo_font_options_destroy (fopt);
}

SCM_DEFINE_PUBLIC (scm_cairo_font_options_equalp, "font-options=?", 2, 0, 0,
                   (SCM this, SCM other),
                   "Returns #t if font options are equivalent")
#define FUNC_NAME s_scm_cairo_font_options_equalp
{
  if (cairo_font_options_equal (scm_to_cairo_font_options (this),
                                scm_to_cairo_font_options (other)))
    return SCM_BOOL_T;
  else
    return SCM_BOOL_F;
}
#undef FUNC_NAME

/**********************************************************************
 * cairo_path_t
 **********************************************************************/

SCM
scm_take_cairo_path (cairo_path_t *path)
{
  SCM spath;

  spath = scm_make_foreign_object_1 (scm_fo_cairo_path_t, path);

  return spath;
}

cairo_path_t*
scm_to_cairo_path (SCM scm)
{
  scm_assert_foreign_object_type (scm_fo_cairo_path_t, scm);
  return (cairo_path_t*)scm_foreign_object_ref (scm, 0);
}

static void
scm_cairo_path_free (SCM fo)
{
  cairo_path_t *path = (cairo_path_t*)scm_foreign_object_ref (fo, 0);

  scm_foreign_object_set_x (fo, 0, NULL);
  cairo_path_destroy (path);
}

#if CAIRO_VERSION >= CAIRO_VERSION_ENCODE(1,10,0)
/**********************************************************************
 * cairo_device_t
 **********************************************************************/

SCM
scm_take_cairo_device (cairo_device_t *device)
{
  SCM sdevice;

  sdevice = scm_make_foreign_object_1 (scm_fo_cairo_device_t, device);

  return sdevice;
}

SCM
scm_from_cairo_device (cairo_device_t *device)
{
  return scm_take_cairo_device (cairo_device_reference (device));
}

cairo_device_t*
scm_to_cairo_device (SCM scm)
{
  scm_assert_foreign_object_type (scm_fo_cairo_device_t, scm);
  return (cairo_device_t*)scm_foreign_object_ref (scm, 0);
}

static void
scm_cairo_device_free (SCM fo)
{
  cairo_device_t *device = (cairo_device_t*)scm_foreign_object_ref (fo, 0);

  scm_foreign_object_set_x (fo, 0, NULL);
  cairo_device_destroy (device);
}


/**********************************************************************
 * cairo_region_t
 **********************************************************************/

SCM
scm_take_cairo_region (cairo_region_t *region)
{
  SCM sregion;

  sregion = scm_make_foreign_object_1 (scm_fo_cairo_region_t, region);

  return sregion;
}

cairo_region_t*
scm_to_cairo_region (SCM scm)
{
  scm_assert_foreign_object_type (scm_fo_cairo_region_t, scm);
  return (cairo_region_t*)scm_foreign_object_ref (scm, 0);
}

static void
scm_cairo_region_free (SCM fo)
{
  cairo_region_t *region = (cairo_region_t*)scm_foreign_object_ref (fo, 0);

  scm_foreign_object_set_x (fo, 0, NULL);
  cairo_region_destroy (region);
}

SCM_DEFINE_PUBLIC (scm_cairo_region_equalp, "region=?", 2, 0, 0,
                   (SCM this, SCM other),
                   "Returns #t if regions are equivalent")
#define FUNC_NAME s_scm_cairo_region_equalp
{
  return scm_from_bool (cairo_region_equal (scm_to_cairo_region (this),
                                            scm_to_cairo_region (other)));
}
#endif /* 1.10 */

void
scm_init_cairo_fo_types (void)
{
#ifndef SCM_MAGIC_SNARFER
#include "guile-cairo-fo-types.x"
#endif
  scm_fo_cairo_t
    = scm_make_foreign_object_type (scm_from_utf8_symbol ("cairo-context"),
                                    scm_list_1 (scm_from_utf8_symbol ("ptr")),
                                    scm_cairo_free);
  scm_variable_set_x (scm_fo_cairo_var, scm_fo_cairo_t);

  scm_fo_cairo_surface_t
    = scm_make_foreign_object_type (scm_from_utf8_symbol ("cairo-surface"),
                                    scm_list_1 (scm_from_utf8_symbol ("ptr")),
                                    scm_cairo_surface_free);
  scm_variable_set_x (scm_fo_cairo_surface_var, scm_fo_cairo_surface_t);

  scm_fo_cairo_pattern_t
    = scm_make_foreign_object_type (scm_from_utf8_symbol ("cairo-pattern"),
                                    scm_list_1 (scm_from_utf8_symbol ("ptr")),
                                    scm_cairo_pattern_free);
  scm_variable_set_x (scm_fo_cairo_pattern_var, scm_fo_cairo_pattern_t);

  scm_fo_cairo_scaled_font_t
    = scm_make_foreign_object_type (scm_from_utf8_symbol ("cairo-scaled-font"),
                                    scm_list_1 (scm_from_utf8_symbol ("ptr")),
                                    scm_cairo_scaled_font_free);
  scm_variable_set_x (scm_fo_cairo_scaled_font_var, scm_fo_cairo_scaled_font_t);

  scm_fo_cairo_font_face_t
    = scm_make_foreign_object_type (scm_from_utf8_symbol ("cairo-font-face"),
                                    scm_list_1 (scm_from_utf8_symbol ("ptr")),
                                    scm_cairo_font_face_free);
  scm_variable_set_x (scm_fo_cairo_font_face_var, scm_fo_cairo_font_face_t);

  scm_fo_cairo_font_options_t
    = scm_make_foreign_object_type (scm_from_utf8_symbol ("cairo-font-options"),
                                    scm_list_1 (scm_from_utf8_symbol ("ptr")),
                                    scm_cairo_font_options_free);
  scm_variable_set_x (scm_fo_cairo_font_options_var, scm_fo_cairo_font_options_t);

  scm_fo_cairo_path_t
    = scm_make_foreign_object_type (scm_from_utf8_symbol ("cairo-path"),
                                    scm_list_1 (scm_from_utf8_symbol ("ptr")),
                                    scm_cairo_path_free);
  scm_variable_set_x (scm_fo_cairo_path_var, scm_fo_cairo_path_t);
  scm_c_export("<cairo-context>", "<cairo-surface>", "<cairo-pattern>",
               "<cairo-scaled-font>", "<cairo-font-face>",
               "<cairo-font-options>", "<cairo-path>", NULL);

#if CAIRO_VERSION >= CAIRO_VERSION_ENCODE(1,10,0)
  scm_fo_cairo_device_t
    = scm_make_foreign_object_type (scm_from_utf8_symbol ("cairo-device"),
                                    scm_list_1 (scm_from_utf8_symbol ("ptr")),
                                    scm_cairo_device_free);
  scm_variable_set_x (scm_fo_cairo_device_var, scm_fo_cairo_device_t);

  scm_fo_cairo_region_t
    = scm_make_foreign_object_type (scm_from_utf8_symbol ("cairo-region"),
                                    scm_list_1 (scm_from_utf8_symbol ("ptr")),
                                    scm_cairo_region_free);
  scm_variable_set_x (scm_fo_cairo_region_var, scm_fo_cairo_region_t);
  scm_c_export ("<cairo-device>", "<cairo-region>", NULL);
#endif /* 1.10 */
}
